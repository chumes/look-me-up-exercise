import get from 'axios';
import { checkIfIp } from '../utils/ip-check'

get(`https://dns-history.whoisxmlapi.com/api/v1?apiKey=at_Ac8UMb2P2scM1I02SFTv0CeJQ7mSC&ip=${process.argv[2]}`)
  .then(res => {
    process.send({ serviceName: 'Reverse DNS', data: res.data });
  })
  .catch(error => {
    if (error.response?.status === 422) {
      process.exit(9);
    } else {
      console.log(error);
    }
  })