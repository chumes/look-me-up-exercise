import * as ping from 'ping';

ping.promise.probe(process.argv[2], {
    timeout: 10,
    extra: ['-i', '2'],
}).then(function (res) {
    process.send({ serviceName: 'PING', data: res });
});