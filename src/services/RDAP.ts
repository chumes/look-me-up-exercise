import get from 'axios';
import { checkIfIp } from '../utils/ip-check'

let requestUrl = checkIfIp(process.argv[2]) ? 'https://www.rdap.net/ip' : 'https://www.rdap.net/domain'

get(`${requestUrl}/${process.argv[2]}`)
  .then(res => {
    process.send({ serviceName: 'RDAP', data: res.data });
  })
  .catch(error => {
    if (error.response?.status === 404) {
      process.exit(9);
    } else {
      console.log(error);
    }
  })