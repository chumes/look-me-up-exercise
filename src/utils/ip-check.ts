export function checkIfIp(stringtoCheck) {
    const ipv4Url = RegExp('^(([1-9]?\\d|1\\d\\d|2[0-5][0-5]|2[0-4]\\d)\\.){3}([1-9]?\\d|1\\d\\d|2[0-5][0-5]|2[0-4]\\d)$');
    return ipv4Url.test(stringtoCheck);
}