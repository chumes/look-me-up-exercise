import { Controller, Get, Param, Query } from '@nestjs/common';
import { AppService } from './app.service';
import * as child from 'child_process';
import { HttpService } from '@nestjs/axios';
import { checkIfIp } from './utils/ip-check'
import * as fs from 'fs'
import * as path from 'path';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private httpService: HttpService) {}

  @Get('/search/:ip')
  getHello(@Param('ip') ip, @Query() query): Promise<any> {
    return new Promise((resolve, reject) => {
      let returnData: any = {};
      if (query.services) {
        query.services = query.services.replace(/\s+/g, '');
      }
      let filesList = query.services ? query.services.split(',') : ['geoip'];
      //let childList = [];

      returnData['Input Type'] = checkIfIp(ip) ? 'IP Address' : 'Domain';
  
      for (let fileName of filesList) {
        const childProcess = child.fork(`src/services/${fileName}.ts`, [ip], { execArgv: ['-r', 'ts-node/register'] });
        childProcess.on('message', (msg: any) => {
          returnData[msg.serviceName] = msg.data;
        });
    
        childProcess.on('exit', function (code) {
          //console.log(`Child process(${fileName}) exited with code: ${code}`);
          if (code === 9) {
            returnData[fileName] = { Error: `Invalid ${checkIfIp(ip) ? 'IP Address' : 'Domain'}` };
          }
          filesList = filesList.filter(file => file !== fileName);
          if (filesList.length === 0) {
            resolve(returnData);
          }
        });
        //childList.push(childProcess);
      }
    });
  }
}
